import tkinter as tk
from tkinter import *
import tkinter.filedialog as fdialog

root_window = tk.Tk()
root_window.title("Scheduler App")
root_window.geometry("1080x720")

my_canvas = Canvas(root_window, bg = "pink")



# Frame contains the "Ok" and "Close" buttons at the bottom of the screen

bottom_button_frame = Frame(my_canvas)
exit_button = tk.Button(bottom_button_frame, text = "Close", width = 10, command = root_window.destroy)
ok_button = tk.Button(bottom_button_frame, text = "Ok", width = 10)


ok_button.pack(side=LEFT)
exit_button.pack(side=LEFT)

# Load files into app

action_frame = Frame(my_canvas)

def UploadAction(event = None):
    filenames = fdialog.askopenfilenames(title = 'Choose a file', filetypes = (("jpeg files", "*.jpg"),("all files","*.*")))
    # print("Selected: \n", filenames)
    # print(type(filenames))
    # print(type(filenames[1]))

def NewFrame():
    action_frame.destroy()
    new_schedule = Frame(my_canvas)
    my_canvas.config(bg = "white")
    
    

import_button = tk.Button(action_frame, text = "Select Files", width = 20, command = UploadAction)

# Add custom schedule

new_schedule_button = tk.Button(action_frame, text = "New Schedule", width = 20, command = NewFrame)




new_schedule_button.pack()


my_canvas.pack(fill = "both", expand = True)
bottom_button_frame.pack(side = BOTTOM)
action_frame.pack()
import_button.pack()

root_window.mainloop()





